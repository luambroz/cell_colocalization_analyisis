import cv2
import functools
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy import stats
from scipy.stats import linregress
from matplotlib.colors import LogNorm

######################################################################
def separate_channel(image, channel):

    channels = [0,1,2]
    channels.remove(channel)
    image_matrix = np.delete(image, np.s_[channels[0],channels[1]], axis=2) #Drop unwanted channels
    zeros = np.zeros((image.shape[0], image.shape[1], 1))
    if channel == 0: new_image = np.concatenate((image_matrix, zeros, zeros), axis = 2) 
    if channel == 1: new_image = np.concatenate((zeros, image_matrix, zeros), axis = 2)
    if channel == 2: new_image = np.concatenate((zeros, zeros, image_matrix), axis = 2)

    return new_image.astype(np.uint8)
######################################################################



######################################################################
def cell_touching_the_edges(cnt, image):

        for coord in cnt:
            if coord[0][0] == 0 or (coord[0][0] == image.shape[0] - 1) or coord[0][1] == 0 or (coord[0][1] == image.shape[1] - 1):
                return True

        return False
######################################################################



######################################################################
def find_clusters(image, area_cell_min, area_cell_max, color_mask = 'r', kernel_blurring_width = 5, blurring_threshold = 8, remove_cells_touching_the_edges = True):

    if   color_mask == 'r': color_scheme = (255, 0, 0)
    elif color_mask == 'g': color_scheme = (0, 255, 0)
    elif color_mask == 'b': color_scheme = (0, 0, 255)
    
    image_copy = copy.deepcopy(image)
    
    gray = cv2.cvtColor(image_copy, cv2.COLOR_RGB2GRAY)
    blurred = cv2.GaussianBlur(gray, (kernel_blurring_width, kernel_blurring_width), 0) #SigmaX is set to 0, so it is computed from the kermel hight and width
    thresh = cv2.threshold(blurred, blurring_threshold, 255, cv2.THRESH_BINARY)[1]

    #Retrieve contours without holes
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    good_contours = []
    for cnt in contours:

        #Evaluate the contour area
        area = cv2.contourArea(cnt)
        if area < area_cell_min or area > area_cell_max:
            continue
        
        #Exclude cells touching the border
        if remove_cells_touching_the_edges:
            if cell_touching_the_edges(cnt, image):
                continue
        
        good_contours.append(cnt)
    
    #Sort contours from largest to smallest
    good_contours.sort(key=lambda cnt: cv2.contourArea(cnt), reverse = True)
        
    return good_contours
######################################################################



######################################################################
def filter_image(image, mask):
    
    """Function that returns the filtered image and the average intensity in the mask. Provide the mask with only one channel non-zero."""
            
    boolean_mask = mask != 0
    image_copy = np.multiply(image, boolean_mask) #Matrix multiplication element by element
    
    non_zeros_elements = np.count_nonzero(boolean_mask)
    if non_zeros_elements > 0:
        average_intensity = np.sum(image_copy) / non_zeros_elements
    else:
        average_intensity = 0
    
    return image_copy.astype(np.uint8), average_intensity
######################################################################



######################################################################
def avg_area(foci):

    if len(foci) > 0:
        areas = np.array([cv2.contourArea(f) for f in foci])
        tot_area = areas.sum()
        return tot_area / len(foci)
    else:
        return 0.
######################################################################


######################################################################
def get_foci(image, foci, color_filter):

    if   color_filter == 'r': color_scheme = (255, 0, 0)
    elif color_filter == 'g': color_scheme = (0, 255, 0)
    elif color_filter == 'b': color_scheme = (0, 0, 255)

    mask_foci = np.zeros((image.shape[0], image.shape[1], 3))
    for f in foci:
        cv2.drawContours(mask_foci, [f], 0, (color_scheme), -1) 

    new_imageRed_onlyfoci, average_intensity_Red_onlyfoci = filter_image(image, mask_foci)

    return new_imageRed_onlyfoci, average_intensity_Red_onlyfoci 
######################################################################



######################################################################
def costes_method(redImage, greenImage, red_thresholds = [10, 5, 2, 1], image_name = ''):

    pixel_red_intensity = np.empty(redImage.shape[0]*redImage.shape[1]) #Empty initialization. For full image: 1024x1024.
    for y in range(redImage.shape[0]): #Standard 1024
        for x in range(redImage.shape[1]): #Standard 1024
                pixel_red_intensity[y * redImage.shape[0] + x] = redImage[y][x][0] #0 is the red channel

    pixel_green_intensity = np.empty(greenImage.shape[0]*greenImage.shape[1]) #Empty initialization. For full image: 1024x1024.
    for y in range(greenImage.shape[0]): #Standard 1024
        for x in range(greenImage.shape[1]): #Standard 1024
                pixel_green_intensity[y * greenImage.shape[0] + x] = greenImage[y][x][1] #1 is the green channel
    
    #Fit to pixel intensities
    plt_2dim = plt.hist2d(x = pixel_red_intensity, y = pixel_green_intensity, range=np.array([(-0.5, 255.5), (-0.5, 255.5)]), bins=[32,32], norm=LogNorm())
    plt.xlabel('Red pixel intensity')
    plt.ylabel('Green pixel intensity')
    plt.colorbar()

    fit = linregress(pixel_red_intensity, pixel_green_intensity) #Fit with a line
    fit_fn = np.poly1d((fit.slope, fit.intercept)) #Create the line
    plot = plt.plot(pixel_red_intensity, fit_fn(pixel_red_intensity)) #Plot the estimate of the green as a function of the red intensity
    plt.savefig(image_name)
    plt.close()
    ###

    #Look for the threshold
    for red_threshold in red_thresholds:

        green_threshold = fit.slope * red_threshold + fit.intercept 
        
        pixel_indices_to_keep = [i for i in range(len(pixel_red_intensity)) if pixel_red_intensity[i] > red_threshold or pixel_green_intensity[i] > green_threshold]
        pixel_red_intensity_below_threshold = [pixel_red_intensity[i] for i in pixel_indices_to_keep]
        pixel_green_intensity_below_threshold = [pixel_green_intensity[i] for i in pixel_indices_to_keep]
           
        r, p0 = stats.pearsonr(pixel_red_intensity_below_threshold, pixel_green_intensity_below_threshold)

        if r < 0.0:
            return red_threshold, green_threshold

    #If a threshold is not found, return 0, 0
    return 0, 0
######################################################################


######################################################################
def get_experiment_and_conditions(image_path):

    conditions = 'unknown'
    if 'par' in image_path:
        conditions = 'par'
    elif 'siBRCA2' in image_path:
        conditions = 'siBRCA2'
    elif 'siSCR' in image_path:
        conditions = 'siSCR'
    elif 'siMisNeg' in image_path:
        conditions = 'siMisNeg'
    elif 'Mock' in image_path:
        conditions = 'Mock'

    if 'Aphi+' in image_path:
        conditions = conditions + '_Aphi+'

    experiment = 'unknown'
    if 'gH2AX-CENPB' in image_path:
        experiment = 'gH2AX-CENPB'
    elif 'gH2AX-CENPC' in image_path:
        experiment = 'gH2AX-CENPC'
    elif 'CENPA-CENPB' in image_path:
        experiment = 'CENPA-CENPB'

    replicate = '0'
    if 'rep1' in image_path:
        replicate = '1'
    elif 'rep2' in image_path:
        replicate = '2'

    return experiment, conditions, replicate
######################################################################



######################################################################
def calculateMCC(redImage, greenImage, redThreshold, greenThreshold):
    
    """Provide the images with the same size with only one (red / green) channel non-zero."""

    red_matrix = np.delete(redImage, np.s_[1,2], axis=2) #Drop green and blue
    green_matrix = np.delete(greenImage, np.s_[0,2], axis=2) #Drop red and blue
    tot_red = red_matrix.sum()
    tot_green = green_matrix.sum()
    red_above_th = red_matrix[green_matrix > greenThreshold].sum()
    green_above_th = green_matrix[red_matrix > redThreshold].sum()

    if tot_red > 0:
        M_red = red_above_th / tot_red
    else:
        M_red = 0

    if tot_green > 0:
        M_green = green_above_th / tot_green
    else:
        M_green = 0

    return M_red, M_green
######################################################################



######################################################################
def shuffle_foci_in_the_nucleus(foci, foci_initial_image, nucleus, color_mask):

    """Move randomly foci in the nucleus. Assume nucleus in the blue channel."""

    if   color_mask == 'r': color_scheme = (255, 0, 0)
    elif color_mask == 'g': color_scheme = (0, 255, 0)
    elif color_mask == 'b': color_scheme = (0, 0, 255)

    image_nucleus = np.delete(nucleus, np.s_[0,1], axis=2) 
    squeezed_image_nucleus = np.squeeze(image_nucleus)
    nucleus_idx = np.nonzero(squeezed_image_nucleus)

    shuffled_foci = np.zeros((nucleus.shape[0], nucleus.shape[1], 3))
    for f in foci:

        f_initial_image = np.zeros((nucleus.shape[0], nucleus.shape[1], 3))
        cv2.drawContours(f_initial_image, [f], 0, color_scheme, -1)
        f_image, _ = filter_image(foci_initial_image, f_initial_image)
        f_idx = np.nonzero(f_image)
        f_idx = np.stack(f_idx, axis=-1)

        random_nucleus_idx = np.random.randint(0, len(nucleus_idx[0]) -1)
        random_nucleus_y_coord = nucleus_idx[0][random_nucleus_idx]
        random_nucleus_x_coord = nucleus_idx[1][random_nucleus_idx]

        M = cv2.moments(f)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        dy = random_nucleus_y_coord - cy
        dx = random_nucleus_x_coord - cx

        for (f_i_y, f_i_x, f_i_c) in f_idx:
            try:
                shuffled_foci[f_i_y+dy][f_i_x+dx][f_i_c] = f_image[f_i_y][f_i_x][f_i_c]
            except:
                pass 

    return shuffled_foci.astype(np.uint8)
######################################################################
