# Description
Scripts for performing cell colocalization analysis of foci.

![](NFLAP-BRCA2-siBRCA2-_Aphi+_gH2AX-CENPC_01.oif_Magnification60.0_Zoom2.0_RGB.png)
# Instructions  
The notebook `ImagePreProcessing.ipynb` segments cells according to the nuclei (blue channel), calculates the Costes thresholds for the green and red channels, and measures Manders coefficients. For each cell, the code allows to shuffle the position of the foci and recalculate the values of the Manders coefficients multiple times. For example, for the largest cell of the image above:
![](picture0_cell0.png)
The notebook `Analysis.ipynb` allows to aggregate the information in plots.
